const square = function(x) {
    return x * x;
};

console.log(square(12));

const makeNoise = function () {
    console.log("Pling!");
};

makeNoise();
console.log(makeNoise);
console.log(makeNoise());

const power = function (base, exponent) {
    let result = 1;
    for (let count = 0; count < exponent; count++) {
        console.log("count before result: " + count);
        console.log("initial result: " + result);
        result *= base;
        console.log("calculated result: " + result);
    }
    return result;
};

console.log(power(2, 10));

let x = 10;

if (true) {
    let y = 20;
    var z = 30;
    console.log(x + y + z);
}

console.log("z:" + z);

const halve = function (n) {
    return n / 2;
};

let n = 10;
console.log(halve(100));
console.log(n);

const hummus = function (factor) {
    const ingredient = function (amount, unit, name) {
        let ingredientAmount = amount * factor;
        if (ingredientAmount > 1) {
            unit += "s";
        }
        console.log(`${ingredientAmount} ${unit} ${name}`);
    };
    ingredient(1, "can", "chickpeas");
};

console.log(hummus());
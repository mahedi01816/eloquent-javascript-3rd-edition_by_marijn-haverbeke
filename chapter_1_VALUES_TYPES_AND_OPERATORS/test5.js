console.log(typeof Infinity);
console.log(typeof Math.LN2);
console.log(typeof (42));
console.log(typeof 3.14);
console.log(typeof 42);
console.log(typeof "blubber");
console.log(typeof true);
console.log(typeof undeclaredVariable);
console.log(typeof NaN);
console.log(typeof Number("1"));
console.log(typeof Number("shoe"));
console.log(typeof 42n);
console.log(typeof '');
console.log(typeof `template literal`);
console.log(typeof "1");
console.log(typeof (typeof 1));
console.log(typeof String(1));
console.log(typeof false);
console.log(typeof Boolean(1));
console.log(typeof !(1));
console.log(typeof !!(1));
console.log(typeof Symbol());
console.log(typeof Symbol("foo"));
console.log(typeof Symbol.iterator);
console.log(typeof {a: 1});
console.log(typeof [1, 2, 4]);
console.log(typeof new Date());
console.log(typeof new Boolean(true));
console.log(typeof new Number(1));
console.log(typeof new String("abc"));
console.log(typeof function (){});
console.log(typeof class C{});
console.log(typeof Math.sin());
console.log(typeof Math.sin);
console.log(typeof Math.cos);
console.log(typeof null);

let string = new String("String");
console.log(typeof string);

let number = new Number(100);
console.log(typeof number);

let func = new Function();
console.log(typeof func);

let iData = 99;
console.log(typeof iData + "wisen");
console.log(typeof (iData + "wisen"));

console.log(typeof /r/);

console.log(typeof newLetVariable);
//let newLetVariable;
//let undeclaredVariable;
console.log(typeof 4.5);
console.log(typeof "x");
console.log(typeof (console.log(typeof 4.5)));
console.log(typeof (typeof 4.5));
console.log(typeof (console.log(3)));
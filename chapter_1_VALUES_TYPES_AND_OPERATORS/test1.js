function factorial(n) {
    if (n === 0) {
        console.log('[if]value of n : ', n);
        return 1;
    } else {
        console.log('[else]value of n : ', n);
        return factorial(n - 1) * n;
    }
}

console.log(factorial(8));
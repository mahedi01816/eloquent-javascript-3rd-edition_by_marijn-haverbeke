console.log(8 * null);
console.log("5" - 1);
console.log("5" + 1);
console.log("five" * 2);
console.log(false == 0);
console.log(8 * null);
console.log(8 + null);
console.log(8 - null);
console.log(8 / null);
console.log("5" - 1);
console.log("5" + 1);
console.log("five" * 2);
console.log("five" / 2);
console.log(false == 0);
console.log(false === 0);
console.log(true == 1);
console.log(true === 1);
console.log(null == undefined);
console.log(null === undefined);
console.log(null == 0);
console.log(null === null);
console.log(undefined === undefined);
console.log(undefined == 0);
console.log(0 == false);
console.log(0 === false);
console.log("" == false);
console.log("" === false);